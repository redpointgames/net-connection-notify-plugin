// Copyright 2020 June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "NotifyNetConnectionReadyInterface.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class UNotifyNetConnectionReadyInterface : public UInterface
{
    GENERATED_BODY()
};

class NETCONNECTIONNOTIFY_API INotifyNetConnectionReadyInterface
{
    GENERATED_BODY()

public:
    UFUNCTION(
        BlueprintCallable,
        BlueprintNativeEvent,
        Category = "Networking",
        Meta = (DisplayName = "On Net Connection Available"))
    void OnNetConnectionAvailable(AController *InController);
};
