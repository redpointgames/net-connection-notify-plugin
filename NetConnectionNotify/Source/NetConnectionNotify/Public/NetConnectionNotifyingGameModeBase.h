// Copyright 2020 June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "NetConnectionNotifyingGameModeBase.generated.h"

/**
 *
 */
UCLASS() class NETCONNECTIONNOTIFY_API ANetConnectionNotifyingGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    // Override the Login handler so we can call the OnNetConnectionAvailable on
    // the PlayerState with the NetConnection pre-emptively set on the controller.
    virtual APlayerController *Login(
        UPlayer *NewPlayer,
        ENetRole InRemoteRole,
        const FString &Portal,
        const FString &Options,
        const FUniqueNetIdRepl &UniqueId,
        FString &ErrorMessage) override;
};
