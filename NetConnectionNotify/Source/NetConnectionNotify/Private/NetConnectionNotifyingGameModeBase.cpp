// Copyright 2020 June Rhodes. All Rights Reserved.

#include "NetConnectionNotifyingGameModeBase.h"

#include "Engine/LocalPlayer.h"
#include "Engine/NetConnection.h"
#include "Engine/Player.h"
#include "GameFramework/GameSession.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/PlayerState.h"
#include "NotifyNetConnectionReadyInterface.h"

APlayerController *ANetConnectionNotifyingGameModeBase::Login(
    UPlayer *NewPlayer,
    ENetRole InRemoteRole,
    const FString &Portal,
    const FString &Options,
    const FUniqueNetIdRepl &UniqueId,
    FString &ErrorMessage)
{
    ErrorMessage = this->GameSession->ApproveLogin(Options);
    if (!ErrorMessage.IsEmpty())
    {
        return nullptr;
    }

    APlayerController *const NewPlayerController = this->SpawnPlayerController(InRemoteRole, Options);
    if (NewPlayerController == nullptr)
    {
        // Handle spawn failure.
        UE_LOG(
            LogGameMode,
            Log,
            TEXT("Login: Couldn't spawn player controller of class %s"),
            PlayerControllerClass ? *PlayerControllerClass->GetName() : TEXT("NULL"));
        ErrorMessage = FString::Printf(TEXT("Failed to spawn player controller"));
        return nullptr;
    }

    // Notify the player state that the network connection is ready.
    if (NewPlayerController->PlayerState->Implements<UNotifyNetConnectionReadyInterface>())
    {
        // Set the NetConnection and Player early so that it can be accessed by blueprints.
        ULocalPlayer *LP = Cast<ULocalPlayer>(NewPlayer);
        if (LP == nullptr)
        {
            NewPlayerController->NetConnection = Cast<UNetConnection>(NewPlayer);
            NewPlayerController->Player = NewPlayer;
        }

        INotifyNetConnectionReadyInterface::Execute_OnNetConnectionAvailable(
            NewPlayerController->PlayerState,
            NewPlayerController);

        if (LP == nullptr)
        {
            NewPlayerController->NetConnection = nullptr;
            NewPlayerController->Player = nullptr;
        }
    }

    // Customize incoming player based on URL options
    ErrorMessage = this->InitNewPlayer(NewPlayerController, UniqueId, Options, Portal);
    if (!ErrorMessage.IsEmpty())
    {
        NewPlayerController->Destroy();
        return nullptr;
    }

    return NewPlayerController;
}




